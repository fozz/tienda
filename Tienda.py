import sys

articulos = {}

def anadir(articulo, precio):
    articulos[articulo] = precio

def mostrar():
    print("Lista de artículos en la tienda:")
    for articulo, precio in articulos.items():
        print(f"{articulo}: {precio} euros")

def pedir_articulo() -> str:
    while True:
        articulo = input("Artículo: ")
        if articulo in articulos:
            return articulo
        else:
            print("El artículo no está en la tienda. Por favor, elige uno de la lista.")

def pedir_cantidad() -> float:
    while True:
        cantidad_str = input("Cantidad: ")
        try:
            cantidad = float(cantidad_str)
            return cantidad
        except ValueError:
            print("Cantidad no válida. Introduce un número válido.")

def main():
    sys.argv.pop(0)  # Elimina el primer argumento que es el nombre del script

    if len(sys.argv) % 2 != 0:
        print("Error en argumentos: Debes proporcionar un nombre y un precio para cada artículo.")
        sys.exit(1)

    for i in range(0, len(sys.argv), 2):
        nombre = sys.argv[i]
        precio_str = sys.argv[i + 1]

        try:
            precio = float(precio_str)
            anadir(nombre, precio)
        except ValueError:
            print(f"Error en argumentos: {precio_str} no es un precio válido para {nombre}.")
            sys.exit(1)

    mostrar()

    compras = {}

    while True:
        articulo = pedir_articulo()
        cantidad = pedir_cantidad()

        if articulo in compras:
            compras[articulo] += cantidad
        else:
            compras[articulo] = cantidad

        continuar = input("¿Deseas comprar otro artículo? (s/n): ")
        if continuar.lower() != 's':
            break

    print("\nResumen de la compra:")
    for articulo, cantidad in compras.items():
        print(f"{cantidad} de {articulo}, a pagar {cantidad * articulos[articulo]} euros")

if __name__ == '__main__':
    main()
